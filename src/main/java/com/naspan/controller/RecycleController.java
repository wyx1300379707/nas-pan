package com.naspan.controller;

import com.naspan.common.Result;
import com.naspan.common.annotation.GlobalInterceptor;
import com.naspan.common.annotation.VerifyParam;
import com.naspan.entity.dto.LoadDataDto;
import com.naspan.entity.po.BaseEntity;
import com.naspan.entity.vo.PageDataVo;
import com.naspan.service.FileInfoService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/recycle")
public class RecycleController {
    @Resource
    private FileInfoService fileInfoService;

    @PostMapping("/loadRecycleList")
    @GlobalInterceptor(checkParam = true)
    public Result loadRecycleList(@VerifyParam(require = true) BaseEntity baseEntity) {
        PageDataVo pageDataVo = fileInfoService.loadRecycleList(baseEntity);
        return Result.success(pageDataVo);
    }

    @PostMapping("/recoverFile")
    @GlobalInterceptor(checkParam = true)
    public Result recoverFile(@VerifyParam(require = true) String fileIds) {
        return fileInfoService.recoverFileByRecycleBatch(fileIds);
    }

    @PostMapping("/delFile")
    @GlobalInterceptor(checkParam = true)
    public Result delFile(@VerifyParam(require = true) String fileIds) {
        return fileInfoService.delFileBatch(fileIds);
    }
}
