package com.naspan.controller;

import com.naspan.common.Result;
import com.naspan.common.annotation.GlobalInterceptor;
import com.naspan.common.annotation.VerifyParam;
import com.naspan.common.config.AppConfig;
import com.naspan.entity.dto.LoadDataDto;
import com.naspan.entity.dto.ShareFileDto;
import com.naspan.entity.po.BaseEntity;
import com.naspan.entity.po.FileShare;
import com.naspan.entity.vo.PageDataVo;
import com.naspan.service.FileInfoService;
import com.naspan.service.FileShareService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/share")
public class FileShareController {
    @Resource
    private FileShareService fileShareService;

    @Resource
    private AppConfig appConfig;

    @PostMapping("/loadShareList")
    @GlobalInterceptor
    public Result loadShareList(@VerifyParam(require = true) BaseEntity baseEntity) {
        PageDataVo pageDataVo = fileShareService.loadShareList(baseEntity);
        return Result.success(pageDataVo);
    }

    @PostMapping("/shareFile")
    @GlobalInterceptor
    public Result shareFile(@VerifyParam(require = true) ShareFileDto shareFileDto) {
        FileShare fileShare = fileShareService.shareFile(shareFileDto);
        return Result.success(fileShare);
    }
}
