package com.naspan.controller;

import com.naspan.common.Enum.FileCategoryEnum;
import com.naspan.common.Result;
import com.naspan.common.annotation.GlobalInterceptor;
import com.naspan.common.annotation.VerifyParam;
import com.naspan.common.config.AppConfig;
import com.naspan.common.constants.Constants;
import com.naspan.common.exception.bizExceptionHandle;
import com.naspan.entity.dto.LoadDataDto;
import com.naspan.entity.dto.UploadFileDto;
import com.naspan.entity.po.FileInfo;
import com.naspan.entity.vo.PageDataVo;
import com.naspan.entity.vo.UploadResultVo;
import com.naspan.service.FileInfoService;


import com.naspan.utils.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/file")
public class FileInfoController {
    @Resource
    private FileInfoService fileInfoService;

    @Resource
    private AppConfig appConfig;

    @PostMapping("/loadDataList")
    @GlobalInterceptor
    public Result loadDataList(LoadDataDto fileInfo) {
        PageDataVo pageDataVo = fileInfoService.loadDataList(fileInfo);
        return Result.success(pageDataVo);
    }

    @PostMapping("/uploadFile")
    @GlobalInterceptor(checkParam = true)
    public Result uploadFile(@VerifyParam UploadFileDto fileDto) {
        UploadResultVo resultVo = fileInfoService.uploadFile(fileDto);
        return Result.success(resultVo);
    }

    @GetMapping("/getImage/${month}/${fileName}")
    @GlobalInterceptor(checkParam = true)
    public void getImage(HttpServletResponse response, @PathVariable("month") String month, @VerifyParam(require = true) @PathVariable("fileName") String fileName) {
        String realPath = appConfig.getFilePath() + month + "/" + fileName;
        String contentType = "image/" + FilenameUtils.getExtension(fileName);
        response.setContentType(contentType);
        response.setHeader("Cache-Control", "max-age=2592000");
        FileUtils.readFile(response, realPath);
    }

    @GetMapping("/ts/getVideoInfo/${fileId}")
    @GlobalInterceptor(checkParam = true)
    public void getFile(HttpServletResponse response, @VerifyParam(require = true) @PathVariable("fileId") String fileId) {
        FileInfo fileInfo = fileInfoService.getById(fileId);
        if (fileInfo == null) {
            throw new bizExceptionHandle("文件不存在");
        }
        String realPath = appConfig.getFilePath() + fileInfo.getFilePath();
        //因为视频切割成m3u8所以要对视频单独处理路径
        if (FileCategoryEnum.VIDEO.getCategory().equals(fileInfo.getFileCategory())) {
            String dbPath = fileInfo.getFilePath().substring(0, fileInfo.getFilePath().lastIndexOf("."));
            realPath = realPath + "/" + dbPath + "/" + Constants.INDEX_M3U8;
        }
        FileUtils.readFile(response, realPath);
    }

    @PostMapping("/newFoloder")
    @GlobalInterceptor(checkParam = true)
    public Result newFoloder(@VerifyParam(require = true) String fileName, @VerifyParam(require = true) String filePid) {

        return fileInfoService.newFoloder(fileName, filePid);
    }

    @PostMapping("/getFolderInfo")
    @GlobalInterceptor(checkParam = true)
    public Result getFolderInfo(@VerifyParam(require = true) String path) {

        return fileInfoService.getFolderInfo(path);
    }

    @PostMapping("/rename")
    @GlobalInterceptor(checkParam = true)
    public Result rename(@VerifyParam(require = true) String fileName,
                         @VerifyParam(require = true) String fileId) {
        return fileInfoService.rename(fileName, fileId);
    }

    @PostMapping("/loadAllFolder")
    @GlobalInterceptor(checkParam = true)
    public Result loadAllFolder(@VerifyParam(require = true) String filePid,
                                @VerifyParam(require = true) String currentFileIds) {
        return fileInfoService.loadAllFolder(filePid, currentFileIds);
    }

    @PostMapping("/changeFileFolder")
    @GlobalInterceptor(checkParam = true)
    public Result changeFileFolder(@VerifyParam(require = true) String fileIds,
                                   @VerifyParam(require = true) String filePid) {
        return fileInfoService.changeFileFolder(fileIds, filePid);
    }

    @PostMapping("/createDownloadUrl")
    @GlobalInterceptor(checkParam = true)
    public Result createDownloadUrl(@VerifyParam(require = true) String fileId) {
        return fileInfoService.createDownloadUrl(fileId);
    }

    @GetMapping("/download/${code}")
    @GlobalInterceptor(checkParam = true)
    public void download(HttpServletResponse response, @VerifyParam(require = true) @PathVariable("code") String code) {
        fileInfoService.download(response, code);
    }

    @PostMapping("/delFile")
    @GlobalInterceptor(checkParam = true)
    public Result delFile(@VerifyParam(require = true) String fileIds) {
        return fileInfoService.removeFile2RecycleBatch(fileIds);
    }
}
