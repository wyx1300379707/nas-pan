package com.naspan;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = {"com.naspan"})
@EnableTransactionManagement
@EnableAsync
@EnableScheduling
@MapperScan("com.naspan.mapper")
public class NasPanApplication {

    public static void main(String[] args) {
        SpringApplication.run(NasPanApplication.class, args);
    }

}
