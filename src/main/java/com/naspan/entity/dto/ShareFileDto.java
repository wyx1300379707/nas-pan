package com.naspan.entity.dto;

import com.naspan.entity.po.FileInfo;
import lombok.Data;

@Data
public class ShareFileDto extends FileInfo {
    Boolean showPp;
    Integer validType;
    String codeType;
}
