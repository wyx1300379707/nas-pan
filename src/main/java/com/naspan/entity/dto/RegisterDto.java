package com.naspan.entity.dto;

import com.naspan.common.Enum.VerifyRegexEnum;
import com.naspan.common.annotation.VerifyParam;
import lombok.Data;

@Data
public class RegisterDto {
    @VerifyParam(require = true,regex = VerifyRegexEnum.EMAIL)
    private String email;
    @VerifyParam(require = true,min = 4,max = 10)
    private String emailCode;
    @VerifyParam(require = true,min = 4,max = 15)
    private String nickName;
    @VerifyParam(require = true,regex = VerifyRegexEnum.PASSWORD)
    private String password;
    @VerifyParam(require = true,min = 4,max = 10)
    private String checkCode;
}
