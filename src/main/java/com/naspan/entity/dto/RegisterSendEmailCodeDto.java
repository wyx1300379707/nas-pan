package com.naspan.entity.dto;

import com.naspan.common.Enum.VerifyRegexEnum;
import com.naspan.common.annotation.VerifyParam;
import lombok.Data;

@Data
public class RegisterSendEmailCodeDto {
    @VerifyParam(require = true,regex = VerifyRegexEnum.EMAIL)
    private String email;
    @VerifyParam(require = true)
    private String checkCode;
    @VerifyParam(require = true)
    private Integer type;
}
