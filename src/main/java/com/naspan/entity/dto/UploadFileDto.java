package com.naspan.entity.dto;

import com.naspan.common.annotation.VerifyParam;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class UploadFileDto {
    MultipartFile file;
    String fileId;
    @VerifyParam(require = true)
    String filePid;
    @VerifyParam(require = true)
    String fileName;
    @VerifyParam(require = true)
    String fileMd5;
    @VerifyParam(require = true)
    Integer chunkIndex;
    @VerifyParam(require = true)
    Integer chunks;
}
