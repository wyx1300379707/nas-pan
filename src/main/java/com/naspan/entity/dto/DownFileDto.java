package com.naspan.entity.dto;

import lombok.Data;

@Data
public class DownFileDto {
    private String code;
    private String filePath;
    private String fileName;
    private String fileId;
}
