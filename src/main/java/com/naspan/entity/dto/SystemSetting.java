package com.naspan.entity.dto;

import lombok.Data;

@Data
public class SystemSetting {
    private String registerMailTitle = "邮箱验证码";
    private String registerMailContent = "你好，你的邮箱验证码是,%s,15分钟有效";
    private Integer registerSpace = 500;
}
