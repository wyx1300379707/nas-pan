package com.naspan.entity.dto;

import com.naspan.common.annotation.VerifyParam;
import com.naspan.entity.po.BaseEntity;
import lombok.Data;

import java.io.Serializable;

@Data
public class LoadDataDto extends BaseEntity implements Serializable {
    @VerifyParam(require = true)
    private String category;
    private String fileNameFuzzy;
}
