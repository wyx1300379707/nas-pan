package com.naspan.entity.vo;

import lombok.Data;

@Data
public class LoginUserVo {
    private String nickName;
    private String userId;
    private Boolean isAdmin;
    private String avatar;
}
