package com.naspan.entity.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 表格分页数据对象
 * 
 * @author ruoyi
 */
@Data
public class PageDataVo implements Serializable
{
    private static final long serialVersionUID = 1L;
    /** 总记录数 */
    private long total;

    /** 列表数据 */
    private List<?> List;

}