package com.naspan.entity.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UploadResultVo implements Serializable {

    private String fileId;
    private String Status;
}
