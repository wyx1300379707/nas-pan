package com.naspan.entity.po;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.naspan.common.annotation.VerifyParam;
import lombok.Data;

/**
 * 文件信息
 * @TableName file_info
 */
@TableName(value ="file_info")
@Data
public class FileInfo implements Serializable{
    /**
     * 文件id
     */
    @TableId
    private String fileId;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 文件md5
     */
    private String fileMd5;

    /**
     * 父级id
     */
    private String filePid;

    /**
     * 文件大小
     */
    private Long fileSize;

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 文件封面
     */
    private String fileCover;

    /**
     * 文件路径
     */
    private String filePath;

    /**
     * 创建时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 最后更新时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime lastUpdateTime;

    /**
     * 0:文件 1：目录
     */
    private Integer folderType;

    /**
     * 文件分类 1视频 2：音频  3：图片 4：文档 5：其他
     */
    private Integer fileCategory;

    /**
     * 文件类型:1:视频 2：音频 3：图片 4：pdf 5:doc 6:excel 7:txt 8:code 9:zip 10:其他
     */
    private Integer fileType;

    /**
     * 0:转码中 1：转码失败 2：转码成功
     */
    private Integer status;

    /**
     * 放入回收站时间
     */
    private Date recoveryTime;

    /**
     * 编辑删除 0:删除 1：回收站 2正常
     */
    private Integer delFlag;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;


}