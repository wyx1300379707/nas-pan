package com.naspan.entity.po;

import com.naspan.common.annotation.VerifyParam;
import lombok.Data;

@Data
public class BaseEntity {
    @VerifyParam(require = true)
    private Integer pageNo;
    @VerifyParam(require = true)
    private Integer pageSize;
}
