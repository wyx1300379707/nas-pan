package com.naspan.common.annotation;

import com.naspan.common.Enum.VerifyRegexEnum;

import java.lang.annotation.*;

@Target({ElementType.FIELD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface VerifyParam {
    boolean require() default false;
    int min() default -1;
    int max() default -1;
    VerifyRegexEnum regex() default VerifyRegexEnum.NO;
}
