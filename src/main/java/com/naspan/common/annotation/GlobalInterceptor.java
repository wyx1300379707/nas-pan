package com.naspan.common.annotation;


import org.springframework.web.bind.annotation.Mapping;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Documented
@Mapping
@Retention(RetentionPolicy.RUNTIME)
public @interface GlobalInterceptor {
    boolean checkParam() default false;

    boolean checkLogin() default true;
}
