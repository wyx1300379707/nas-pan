package com.naspan.common.Enum;

import lombok.Getter;

@Getter
public enum FileFolderTypeEnum {
    FILE(0,"文件"),
    FOLDER(1,"目录");
    private Integer type;
    private String desc;
    FileFolderTypeEnum(Integer type, String desc){
        this.type = type;
        this.desc = desc;
    }
}
