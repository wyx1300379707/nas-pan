package com.naspan.common.Enum;

import lombok.Getter;
import org.apache.commons.lang3.ArrayUtils;

@Getter
public enum FileTypeEnum {
    VIDEO(FileCategoryEnum.VIDEO, 1, new String[]{".mp4", ".avi", ".rmvb", ".mkv", ".mov"}, "视频"),
    MUSIC(FileCategoryEnum.MUSIC, 2, new String[]{".mp3", ".wav", ".wma", ".mp2", ".flac", ".midi", ".ra", ".ape"}, "音频"),
    IMAGE(FileCategoryEnum.IMAGE, 3, new String[]{".jpeg", ".jpg", ".png", ".gif", ".bmp", ".dds", ".psd", ".pdt", ".webp", ".xmp", ".svg", ".tiff"}, "图片"),
    PDF(FileCategoryEnum.DOC, 4, new String[]{".pdf"}, "pdf"),
    WORD(FileCategoryEnum.DOC, 5, new String[]{".doc", ".docx"}, "word"),
    EXCEL(FileCategoryEnum.DOC, 6, new String[]{".xlsx", ".xls"}, "excel"),
    TXT(FileCategoryEnum.DOC, 7, new String[]{".txt"}, "txt文本"),
    CODE(FileCategoryEnum.OTHERS, 8, new String[]{".cpp", ".cc", ".c++", ".css", ".dll", ".java", ".class", ".js", ".ts", ".scss", ".vue", ".jxs", ".sql", ".md", ".json", ".xml", ".html", ".hpp"}, "code"),
    ZIP(FileCategoryEnum.OTHERS, 9, new String[]{".rar", ".zip", "7z", ".tar", ".gz"}, "压缩包"),
    OTHERS(FileCategoryEnum.OTHERS, 10, new String[]{".exe"}, "其他");
    private FileCategoryEnum category;
    private Integer type;
    private String[] suffix;
    private String desc;

    FileTypeEnum(FileCategoryEnum category, Integer type, String[] suffix, String desc) {
        this.category = category;
        this.type = type;
        this.suffix = suffix;
        this.desc = desc;
    }
    public static FileTypeEnum getFileTypeBySuffix(String suffix){
        for (FileTypeEnum item : FileTypeEnum.values()) {
            if(ArrayUtils.contains(item.getSuffix(),suffix)){
                return item;
            }
        }
        return null;
    }
}
