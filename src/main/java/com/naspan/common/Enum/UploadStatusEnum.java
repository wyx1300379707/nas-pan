package com.naspan.common.Enum;

import lombok.Getter;

@Getter
public enum UploadStatusEnum {
    UPLOAD_SECONDS("upload_seconds", "妙传"),
    UPLOADING("uploading", "上传中"),
    UPLOAD_FINISH("upload_finish", "上传完成");
    private String code;
    private String desc;

    UploadStatusEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
