package com.naspan.common.exception;

import com.naspan.common.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandle {
    @ExceptionHandler(bizExceptionHandle.class)
    public Result bizExceptionHandle(bizExceptionHandle ex) {
        return Result.error(ex.getCode(),ex.getMsg());
    }
}
