package com.naspan.common.exception;

import com.naspan.common.Enum.ResponseCodeEnum;
import lombok.Getter;

@Getter
public class bizExceptionHandle extends RuntimeException{
    private Integer code;
    private String msg;
    public bizExceptionHandle(){};
    public bizExceptionHandle(ResponseCodeEnum responseCodeEnum){
       this.code  = responseCodeEnum.getCode();
       this.msg = responseCodeEnum.getMsg();
    }
    public bizExceptionHandle(String message){
        this.code  = 500;
        this.msg = message;
    }
}
