package com.naspan.common.constants;

public class Constants {
    public static final Integer REGISTER_CODE_TTL = 5;
    public static final Integer Email_CODE_TTL = 15;
    public static final String REGISTER_CODE = "register:code:";
    public static final String Email_CODE = "email:code:";
    public static final String USER_SPACE = "user:space:";
    public static final String USER_SPACE_TEMP = "user:space:temp:";
    public static final String SESSION_KEY = "session_key";
    public static final String PNG = ".png";
    public static final String INDEX_M3U8 = "index.m3u8";
    public static final String DOWN_FILE_CODE = "down:file:code:";
    public static final String ROOT_FILE_PID = "0";
    public static final String AUTO_RENAME = "(01)";
}
