package com.naspan.common.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppConfig {
    @Value("${spring.mail.username}")
    private String emailForm;

    @Value("${admin.emails}")
    private String adminEmail;

    @Value("${project.folder}")
    private String profile;

    public String getEmailForm() {
        return emailForm;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public String getAvatarPath() {
        return getFilePath() + "/avatar/";
    }
    public String getFilePath() {
        return profile + "/file";
    }
    public String getTempPath() {
        return profile + "/temp/";
    }
}
