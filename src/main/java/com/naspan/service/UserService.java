package com.naspan.service;

import com.naspan.common.Result;
import com.naspan.entity.dto.RegisterDto;
import com.naspan.entity.dto.RegisterSendEmailCodeDto;
import com.naspan.entity.po.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.naspan.entity.vo.LoginUserVo;

/**
* @author Administrator
* @description 针对表【user(用户表)】的数据库操作Service
* @createDate 2023-09-30 14:09:16
*/
public interface UserService extends IService<User> {

    void sendEmailCode(RegisterSendEmailCodeDto registerSendEmailCodeDto);

    Result registerHandle(RegisterDto registerDto);

    LoginUserVo loginHandle(String email, String password, String code);

    void resetPassword(String email, String emailCode, String password);

    Result getUseSpace();
}
