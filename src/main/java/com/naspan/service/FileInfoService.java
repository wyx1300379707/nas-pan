package com.naspan.service;

import com.naspan.common.Result;
import com.naspan.entity.dto.LoadDataDto;
import com.naspan.entity.dto.UploadFileDto;
import com.naspan.entity.po.BaseEntity;
import com.naspan.entity.po.FileInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.naspan.entity.vo.PageDataVo;
import com.naspan.entity.vo.UploadResultVo;

import javax.servlet.http.HttpServletResponse;

/**
* @author Administrator
* @description 针对表【file_info(文件信息)】的数据库操作Service
* @createDate 2023-10-10 11:01:55
*/
public interface FileInfoService extends IService<FileInfo> {

    UploadResultVo uploadFile(UploadFileDto fileDto);

    PageDataVo loadDataList(LoadDataDto fileInfo);

    Result newFoloder(String fileName, String filePid);

    Result getFolderInfo(String path);

    Result rename(String fileName, String fileId);

    Result loadAllFolder(String filePid, String currentFileIds);

    Result changeFileFolder(String fileIds, String filePid);

    Result createDownloadUrl(String fileId);

    void download(HttpServletResponse response,String code);

    Result removeFile2RecycleBatch(String fileIds);

    PageDataVo loadRecycleList(BaseEntity baseEntity);

    Result recoverFileByRecycleBatch(String fileIds);

    Result delFileBatch(String fileIds);
}
