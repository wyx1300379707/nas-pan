package com.naspan.utils;

import com.naspan.common.exception.bizExceptionHandle;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FFmpegUtils {
    public static final Logger logger = LoggerFactory.getLogger(FFmpegUtils.class);

    public static void cutFileVideo(String fileId, String targetFilePath) {
        File targetFile = new File(targetFilePath);
        if (!targetFile.exists()) {
            throw new bizExceptionHandle("视频不存在");
        }
        //生成同名目录存放切割的视频
        File cutFilePath = new File(targetFilePath.substring(0, targetFilePath.lastIndexOf(".")));
        if (!cutFilePath.exists()) {
            cutFilePath.mkdirs();
        }
        List<String> command = new ArrayList<>();
        command.add("ffmpeg");
        command.add("-i");
        command.add(targetFilePath);
        command.add("-c:v");
        command.add("copy");
        command.add("-c:a");
        command.add("copy");
        command.add("-f");
        command.add("ssegment");
        command.add("-segment_format");
        command.add("mpegts");
        command.add("-segment_list");
        command.add(cutFilePath.getPath() + "/index.m3u8");
        command.add("-segment_time");
        command.add("30");
        command.add(cutFilePath.getPath() + "/" + fileId + "%05d.ts");
        String result = ProcessUtil.execute(command);
        logger.info(result);
    }

    public static void cutFileVideoCover(String targetFilePath, int width, String covePath) {
        List<String> command = new ArrayList<>();
        command.add("ffmpeg");
        command.add("-i");
        command.add(targetFilePath);
        command.add("-y");
        command.add("-vframes");
        command.add("1");
        command.add("-vf");
        command.add("scale=" + width + ":" + width + "/a");
        command.add(covePath);
        String result = ProcessUtil.execute(command);
        logger.info(result);
    }

    public static Boolean createThumbnailWidthFFmpeg(File sourceFile, int thumbnailWidth, File targetFile, Boolean delSource) {
        try {
            BufferedImage src = ImageIO.read(sourceFile);
            int width = src.getWidth();
            if (width <= thumbnailWidth) {
                return false;
            }
            compressImage(sourceFile, thumbnailWidth, targetFile, delSource);
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private static void compressImage(File sourceFile, int thumbnailWidth, File targetFile, Boolean delSource) {
        try {
            List<String> command = new ArrayList<>();
            command.add("ffmpeg");
            command.add("-i");
            command.add(sourceFile.getAbsolutePath());
            command.add("-vf");
            command.add("scale=" + thumbnailWidth + ":" + thumbnailWidth + "/a");
            command.add(targetFile.getAbsolutePath());
            String result = ProcessUtil.execute(command);
            if (delSource) {
                FileUtils.forceDelete(sourceFile);
            }
            logger.info(result);
        } catch (Exception e) {
            logger.info("压缩图片失败");
            e.printStackTrace();
        }
    }
}

