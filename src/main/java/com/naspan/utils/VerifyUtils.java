package com.naspan.utils;

import com.naspan.common.Enum.VerifyRegexEnum;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VerifyUtils {
    public static Boolean verify(String refs, String value) {
        if (StringUtils.isEmpty(value)) {
            return false;
        }
        Pattern pattern = Pattern.compile(refs);
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }

    public static Boolean verify(VerifyRegexEnum regexEnum, String value) {
        return verify(regexEnum.getRegex(), value);
    }
}
