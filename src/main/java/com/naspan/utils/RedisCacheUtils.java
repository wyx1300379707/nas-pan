package com.naspan.utils;


import java.util.concurrent.TimeUnit;

public class RedisCacheUtils {
    /**
     * 间隔符
     */
    public static final String SEPARATOR = ":";
    /**
     * 通配符
     */
    public static final String WILDCARD = "*";
    /**
     * 站房缓存 存储周期
     */
    public static final int DAY_TIME =1 ;
    /**
     * 获取缓存
     * @return
     */
    public static RedisCache getRedisConfig(){

        return SpringUtils.getBean(RedisCache.class);
    }

    /**
     * 获取缓存
    * @param tables 参数键值表名
    * @param id 参数id
     * @return
     */
    private static String getCacheKey(String tables,String id){
        return tables+SEPARATOR+id;
    }
    /**
     * 设置站房、设备信息等缓存
     *
     * @param tables 参数键值表名
     * @param id 参数id
     * @param obj 存储的对象
     */
    public static void setRoomCache(String tables,String id,Object obj)
    {
        getRedisConfig().setCacheObject(getCacheKey(tables,id), obj,DAY_TIME, TimeUnit.DAYS);
    }

    /**
     * 获取站房、设备信息等缓存
     *
     * @param tables 参数键
     * @param id 参数id
     * @return Object 返回对象
     */
    public static Object getDictCache(String tables,String id)
    {
      if(getRedisConfig().isCacheObject(getCacheKey(tables,id))){
          return getRedisConfig().getCacheObject(getCacheKey(tables,id));
      }
      return null;
    }
    /**
     * 删除指定站房、设备等缓存
     * @param tables 参数键
     * @param id 参数id
     */
    public static void removeRoomCache(String tables,String id)
    {
        getRedisConfig().deleteObject(getCacheKey(tables,id));
    }

}
