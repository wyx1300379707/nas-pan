package com.naspan.utils;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.naspan.entity.vo.PageDataVo;


public class PageUtils {
    public static PageDataVo getPageList(Page<?> page) {
        PageDataVo pageDataVo = new PageDataVo();
        pageDataVo.setList(page.getRecords());
        pageDataVo.setTotal(page.getTotal());
        return pageDataVo;
    }
}
