package com.naspan.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class ProcessUtil {
    public static String execute(List<String> command) {
        StringBuilder inputStringBuffer = new StringBuilder();
        try {
            ProcessBuilder builder = new ProcessBuilder(command);
            Process process = builder.start();
            System.out.println("============inputStream============");

            // 处理InputStream
            Thread t1 = new Thread(() -> {
                try (InputStream input = process.getInputStream();
                     InputStreamReader reader = new InputStreamReader(input);
                     BufferedReader buffer = new BufferedReader(reader)) {
                    String inputLine;
                    while ((inputLine = buffer.readLine()) != null) {
                        System.out.println(inputLine);
                        inputStringBuffer.append(inputLine);
                    }
                    // 退出循环后表示结束流
                    System.out.println("===》》退出循环后表示结束流");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            t1.start();
            /**
             * 只会存在一个输入流返回
             */
            return inputStringBuffer.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
