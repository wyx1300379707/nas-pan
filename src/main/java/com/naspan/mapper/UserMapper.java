package com.naspan.mapper;

import com.naspan.entity.po.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【user(用户表)】的数据库操作Mapper
* @createDate 2023-09-30 14:09:16
* @Entity com.naspan.entity.pojo.User
*/
public interface UserMapper extends BaseMapper<User> {

    Long getUseSpace(String userId);

    void updateUseSpace(String userId,String useSpace);
}




