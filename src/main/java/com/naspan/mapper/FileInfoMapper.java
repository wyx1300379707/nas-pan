package com.naspan.mapper;

import com.naspan.entity.po.FileInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
* @author Administrator
* @description 针对表【file_info(文件信息)】的数据库操作Mapper
* @createDate 2023-10-10 11:01:55
* @Entity com.naspan.entity.po.FileInfo
*/
public interface FileInfoMapper extends BaseMapper<FileInfo> {

    Integer updateUseSpace(@Param("userId") String userId,@Param("useSpace") Long useSpace,@Param("totalSpace")Long totalSpace);
}




